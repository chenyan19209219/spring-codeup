package com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp;

import com.alibaba.common.lang.StringUtil;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.taobao.igraph.client.model.QueryResult;
import com.taobao.matrix.dao.CreativeDAO;
import com.taobao.recommendplatform.protocol.domain.summary.SummarySearchItem;
import com.taobao.recommendplatform.protocol.service.ServiceFactory;
import com.taobao.recommendplatform.protocol.solution.Context;
import com.taobao.recommendplatform.protocol.solution.RecommendResult;
import com.taobao.recommendplatform.protocol.solution.RecommendSolution;
import com.taobao.recommendplatform.solutions.jgzktm4e.common.framework2.BaseSolution;
import com.taobao.recommendplatform.solutions.jgzktm4e.common.generic.LogUtil;
import com.taobao.recommendplatform.solutions.jgzktm4e.common.reclib.util.Pair;
import com.taobao.recommendplatform.solutions.jgzktm4e.common.util.ParameterUtil;
import com.taobao.recommendplatform.solutions.jgzktm4e.common.util.StopWatch;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.detail.BeerusDetails;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.detail.DispatchSpetialExecuteFactory;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.model.GremlinReader;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.model.MyContext;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.reader.*;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.reader.utils.KgUserFilter;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.tools.LogXGSync;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.tools.MatrixRetShaperTemp;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.tools.SummaryUtiil;
import com.taobao.recommendplatform.solutions.jgzktm4e.matrix_tpp.worker.*;

import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by panxiaofeng on 2018/1/9.
 */
public class MatrixTppSolution extends BaseSolution implements RecommendSolution {
    public static final String MATRIX_ITEM_BE_TASK_NAME = "MATRIX_ITEM_BE_READ_TASK";
    public static final String MATRIX_ITEM_BE_VEC_TASK_NAME = "MATRIX_ITEM_BE_VEC_READ_TASK";
    public static final String COMMON_CONTENT_BE_TASK_NAME = "COMMON_CONTENT_BE_READ_TASK";
    public static final String COMMON_CONTENT_BE_VIDEO_TASK_NAME = "COMMON_CONTENT_BE_VIDEO_READ_TASK";
    public static final String MATRIX_BANNER_BE_TASK_NAME = "MATRIX_BANNER_BE_READ_TASK";
    public static final String MATRIX_BANNER_BE_VEC_TASK_NAME = "MATRIX_BANNER_BE_VEC_READ_TASK";
    public static final String MATRIX_BANNER_BE_U2C_TASK_NAME = "MATRIX_BANNER_BE_U2C_READ_TASK";
    public static final String MATRIX_LIVE_FEED_TASK_NAME = "open_live";
    public static final String MATRIX_ACTIVITY_TASK_NAME = "open_dacu";
    public static final String MATRIX_SHOW_TASK_NAME = "open_show";
    public static final String MATRIX_SHOP_TASK_NAME = "open_shop";
    public static final String MATRIX_HEADLINE_TASK_NAME = "open_headline";
    public static final String MATRIX_YOUHUI_TASK_NAME = "open_youhui";
    public static final String MATRIX_DUMMY_TASK_NAME = "dummy";
    private static final String MATRIX_GENERAL_CTRL_TASK_NAME = "open_ctrl";

    @Override
    public void recommend(Context context, RecommendResult result) throws Exception {
        String logHeader = "[MatrixTppSolution.recommend()]";
        LogUtil.init(context);
        if (context == null) {
            if(LogUtil.isDebugEnable())LogUtil.error("Context is null");
            return;
        }

        if(LogUtil.isDebugEnable())LogUtil.debug("Matrix tpp solution start");
        double startTs = System.currentTimeMillis() / 1000.0;
        Long startTsLong = System.currentTimeMillis();
        StopWatch sw = StopWatch.CreateStopWatchAndStart();
        try {
            MyContext myContext = new MyContext(context);

            if (myContext.getUrlParameter().getScene().equals("7511")) {
                myContext.getUrlParameter().setNeedCommonContent(
                        myContext.getUrlParameter().isNeedCommonContent() & myContext.getBtsParameter().isNeedCrowdCommonContent());
            }


            {
                Long rt = sw.click();
                if (myContext.getBtsParameter().isRtCostRecord()) {
                    // rt trace log
                    MtxTrackLogWorker.init(myContext.getUrlParameter().getPvid());
                    //MtxTrackLogWorker.addKv("build_model_cost", rt);
                    //MtxTrackLogWorker.addKv("item_num", myContext.getUserModel().getItemTriggerList().size());
                    //MtxTrackLogWorker.addKv("cate_num", myContext.getUserModel().getCateTiggerList().size());
                    MtxTrackLogWorker.addKv("user_id", myContext.getUserModel().getUserId());
                    MtxTrackLogWorker.addKv("concept_list", myContext.getUserModel().getConceptInfos());
                }
                if(LogUtil.isDebugEnable())LogUtil.info("{}, MyContext build cost {} ms!", logHeader, rt);
            }
            double ctxInitTs = System.currentTimeMillis() / 1000.0;

            // god's wish
            List<CreativeDAO> godResList = null;
            try {
                GodsWishWorker godsWishWorker = new GodsWishWorker();
                godResList = godsWishWorker.getResponse(
                        myContext.getUserModel().getActIdstIds(), myContext);
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, got godList = [{}]", logHeader, godResList);
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, GodsWishWorker failed {}",
                        logHeader, Throwables.getStackTraceAsString(e));
            }
            // reader containers
            MatrixBuyDirectWorker matrixBuyDirectWorker = new MatrixBuyDirectWorker();
            if(LogUtil.isDebugEnable())LogUtil.debug("getIgraphRetAsync trigger {}, concept {}",
                    myContext.getUserModel().getGremlinItemTriggerList(),
                    myContext.getUserModel().getConceptInfos());
            matrixBuyDirectWorker.getIgraphRetAsync(
                    myContext.getUserModel().getGremlinItemTriggerList(),
                    myContext.getUserModel().getConceptInfos(), myContext);
            Map<String, MatrixReader> matrixReaderMap = new LinkedHashMap<>();
            Map<String, MatrixReader> matrixOpenReaderMap = new LinkedHashMap<>();
            Map<String, MatrixReader> matrixGeneralReaderMap = Maps.newHashMap();

            new MtxCtrlTPPReader().regAndRun(MATRIX_GENERAL_CTRL_TASK_NAME, myContext, matrixGeneralReaderMap);
            // the real matrix interface
            // new DummyReader().regAndRun(MATRIX_DUMMY_TASK_NAME, myContext, matrixReaderMap);
            //if (myContext.getBtsParameter().isContent_type_Split()) {
            //new CommonContentNonVideoBEReader().regAndRun(COMMON_CONTENT_BE_TASK_NAME, myContext, matrixReaderMap);
            //} //else {
            //new CommonContentVideoBEReader().regAndRun(COMMON_CONTENT_BE_VIDEO_TASK_NAME, myContext, matrixReaderMap);
            //new CommonContentNonVideoBEReader().regAndRun(COMMON_CONTENT_BE_TASK_NAME, myContext, matrixReaderMap);
            //new CommonContentBEReader().regAndRun(COMMON_CONTENT_BE_TASK_NAME, myContext, matrixReaderMap);
            //}
            new MatrixItemBEReader().regAndRun(MATRIX_ITEM_BE_TASK_NAME, myContext, matrixReaderMap);

            new CommonContentVideoBEReader().regAndRun(COMMON_CONTENT_BE_VIDEO_TASK_NAME, myContext, matrixReaderMap);

            new MatrixBannerBEReaderRefactor().regAndRun(MATRIX_BANNER_BE_TASK_NAME, myContext, matrixReaderMap);
            // U2C 鍙洖
            if (myContext.getUrlParameter().isNeedMatrixBanner() && myContext.getBtsParameter().getUseU2cRecall() > 0) {
                new MatrixBannerBEU2cReader().regAndRun(MATRIX_BANNER_BE_U2C_TASK_NAME, myContext, matrixReaderMap);
            }
            if (myContext.getBtsParameter().isUserSeqVecMatch()) {
                myContext.getUserModel().readUserVector(myContext.getBtsParameter());
                new MatrixBannerBEReaderRefactor().regAndRun(MATRIX_BANNER_BE_VEC_TASK_NAME, myContext, matrixReaderMap);
                new MatrixItemVecBEReader().regAndRun(MATRIX_ITEM_BE_VEC_TASK_NAME, myContext, matrixReaderMap);
            }

            // activate & read async all needed readers
            // split request to open interface and internal matrix interface
            final long begOpenTsMilis = System.currentTimeMillis();
            if (myContext.getBtsParameter().isUseOpenApi()) {
                new SampleTPPReader().regAndRun(MATRIX_LIVE_FEED_TASK_NAME, myContext, matrixOpenReaderMap);
            }

            int showRatio = myContext.getBtsParameter().getShowRatio();
            int showRndNum = ThreadLocalRandom.current().nextInt(100);
            if (myContext.getBtsParameter().isUseOpenApi() && myContext.getBtsParameter().isUseOpenApiShow()
                    && showRatio >= showRndNum) {
                new SampleTPPReader().regAndRun(MATRIX_SHOW_TASK_NAME, myContext, matrixOpenReaderMap);
            }

            int headLineRatio = myContext.getBtsParameter().getHeadLineRatio();
            int headLineRndNum = ThreadLocalRandom.current().nextInt(100);
            if (myContext.getBtsParameter().isUseOpenApi() && myContext.getBtsParameter().isUseOpenApiHeadline()
                    && headLineRatio >= headLineRndNum) {
                new SampleTPPReader().regAndRun(MATRIX_HEADLINE_TASK_NAME, myContext, matrixOpenReaderMap);
            }

            int youhuiRatio = myContext.getBtsParameter().getYouHuiRatio();
            int youhuiRndNum = ThreadLocalRandom.current().nextInt(100);
            if (myContext.getBtsParameter().isUseOpenApi() && myContext.getBtsParameter().isUseOpenApiYouHui()
                    && youhuiRatio >= youhuiRndNum) {
                new SampleTPPReader().regAndRun(MATRIX_YOUHUI_TASK_NAME, myContext, matrixOpenReaderMap);
            }

            int shopRatio = myContext.getBtsParameter().getShopRatio();
            int shopRndNum = ThreadLocalRandom.current().nextInt(100);
            if (myContext.getBtsParameter().isUseOpenApi() && myContext.getBtsParameter().isUseOpenApiShop()
                    && shopRatio >= shopRndNum) {
                new SampleTPPReader().regAndRun(MATRIX_SHOP_TASK_NAME, myContext, matrixOpenReaderMap);
            }

            if (myContext.getBtsParameter().isUseOpenActivityApi()) {
                new SampleTPPReader().regAndRun(MATRIX_ACTIVITY_TASK_NAME, myContext, matrixOpenReaderMap);
            }

            // all lists
            List<CreativeDAO> mergedCreativeList = Lists.newArrayList();
            List<CreativeDAO> matrixItemCreativeList = Lists.newArrayList();
            List<CreativeDAO> matrixItemVecCreativeList = Lists.newArrayList();
            List<CreativeDAO> commonContentCreativeList = Lists.newArrayList();
            List<CreativeDAO> matrixBannerCreativeList = Lists.newArrayList();
            List<CreativeDAO> matrixBannerVecCreativeList = Lists.newArrayList();
            List<CreativeDAO> matrixBannerU2cCreativeList = Lists.newArrayList();
            List<CreativeDAO> openLiveFeedCreativeList = Lists.newArrayList();
            List<CreativeDAO> openActivityCreativeList = Lists.newArrayList();
            List<CreativeDAO> openShowCreativeList = Lists.newArrayList();
            List<CreativeDAO> openShopCreativeList = Lists.newArrayList();
            List<CreativeDAO> openHeadlineCreativeList = Lists.newArrayList();
            List<CreativeDAO> openYouHuiCreativeList = Lists.newArrayList();
            List<CreativeDAO> openCreativeList = Lists.newArrayList();

            // rotate to get all result
            try {

                long maxTimeOut = myContext.getBtsParameter().getBeTimeOutAll();

                if (myContext.getBtsParameter().isDebugMode()) {
                    maxTimeOut = 500;
                }

                Map<String, List<CreativeDAO>> mtxResQue
                        = MatrixReader.getAllResult(matrixReaderMap, maxTimeOut, myContext);
                for (String taskName : mtxResQue.keySet()) {
                    List<CreativeDAO> oneRet = mtxResQue.get(taskName);
                    switch (taskName) {
                        case MATRIX_BANNER_BE_TASK_NAME: {
                            if(LogUtil.isDebugEnable())LogUtil.debug("{}, banner result is {}", logHeader, oneRet);
                            matrixBannerCreativeList.addAll(oneRet);
                            break;
                        }
                        case MATRIX_BANNER_BE_VEC_TASK_NAME: {
                            matrixBannerVecCreativeList.addAll(oneRet);
                            break;
                        }
                        case MATRIX_BANNER_BE_U2C_TASK_NAME: {
                            matrixBannerU2cCreativeList.addAll(oneRet);
                            break;
                        }
                        case COMMON_CONTENT_BE_TASK_NAME: {
                            commonContentCreativeList.addAll(oneRet);
                            break;
                        }
                        case COMMON_CONTENT_BE_VIDEO_TASK_NAME: {
                            commonContentCreativeList.addAll(oneRet);
                            break;
                        }
                        case MATRIX_ITEM_BE_TASK_NAME: {
                            matrixItemCreativeList.addAll(oneRet);
                            break;
                        }
                        case MATRIX_ITEM_BE_VEC_TASK_NAME: {
                            matrixItemVecCreativeList.addAll(oneRet);
                            break;
                        }
                        case MATRIX_DUMMY_TASK_NAME: {
                            if(LogUtil.isDebugEnable())LogUtil.debug("{}, dummy result is {}", logHeader, oneRet);
                            break;
                        }
                        default:
                            break;
                    }
                }
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{} get result queue failed {}",
                        logHeader, Throwables.getStackTraceAsString(e));
            }
            //commonContentCreativeList = getMockData(myContext, commonContentCreativeList);
            // merge banner creative
            if (myContext.getBtsParameter().isUserSeqVecMatch() && matrixBannerVecCreativeList.size() > 0) {
                List<CreativeDAO> mergeResult = Lists.newArrayList();
                Map<String, CreativeDAO> uniqCreatives = Maps.newHashMap();
                //todo: now i2i first, watch incre
                for (CreativeDAO dao : matrixBannerCreativeList) {
                    if (!uniqCreatives.containsKey(dao.getCreative_id_uniq())) {
                        mergeResult.add(dao);
                        uniqCreatives.put(dao.getCreative_id_uniq(), dao);
                    }
                }
                for (CreativeDAO dao : matrixBannerVecCreativeList) {
                    if (!uniqCreatives.containsKey(dao.getCreative_id_uniq())) {
                        mergeResult.add(dao);
                        uniqCreatives.put(dao.getCreative_id_uniq(), dao);
                    }
                }
                matrixBannerCreativeList = mergeResult;
            }

            // merge item creative
            if (myContext.getBtsParameter().isUserSeqVecMatch() && matrixItemVecCreativeList.size() > 0) {
                List<CreativeDAO> mergeResult = Lists.newArrayList();
                Map<String, CreativeDAO> uniqCreatives = Maps.newHashMap();
                //todo: now i2i first, watch incre
                for (CreativeDAO dao : matrixItemCreativeList) {
                    if (!uniqCreatives.containsKey(dao.getCreative_id_uniq())) {
                        mergeResult.add(dao);
                        uniqCreatives.put(dao.getCreative_id_uniq(), dao);
                    }
                }
                for (CreativeDAO dao : matrixItemVecCreativeList) {
                    if (!uniqCreatives.containsKey(dao.getCreative_id_uniq())) {
                        mergeResult.add(dao);
                        uniqCreatives.put(dao.getCreative_id_uniq(), dao);
                    }
                }
                matrixItemCreativeList = mergeResult;
            }

            double readerParseTs = System.currentTimeMillis() / 1000.0;

            for (ListIterator<CreativeDAO> bannerIter = matrixBannerCreativeList.listIterator(); bannerIter.hasNext(); ) {
                CreativeDAO dao = bannerIter.next();
                if (myContext.getUserModel().getLuxBannerIds().contains(dao.getCreative_id().toString())) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, lux banner removed {}", logHeader, dao.getCreative_id_uniq());
                    bannerIter.remove();
                }
            }

            // -------------------- rtp score and sort --------------------
            RTPWorker rtpWorker = new RTPWorker();

            //


            Map<String, Long> detailTypeMap = Maps.newHashMap();
            for (CreativeDAO dao : matrixBannerCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".before_rtp";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            KgUserFilter kgUserFilter = new KgUserFilter();
            Future<QueryResult> kgUpFltAsync = null;
            if (myContext.getBtsParameter().getUseBannerUpFilter() > 10) {
                try {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{} before filter kg", logHeader);
                    kgUpFltAsync = kgUserFilter.readKgUPInfoAsync(
                            myContext.getUserModel().getUserProfileMap(), matrixBannerCreativeList, myContext);
                } catch (Exception e) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{} read kg filter info failed",
                            logHeader, Throwables.getStackTraceAsString(e));
                }
            }

            if(LogUtil.isDebugEnable())LogUtil.debug("myContext.getBtsParameter().getUseBannerUpFilter() {}", myContext.getBtsParameter().getUseBannerUpFilter());
            if (myContext.getBtsParameter().getUseBannerUpFilter() > 10) {
                if(LogUtil.isDebugEnable())LogUtil.debug("yjlin: matrixBannerCreativeList {}", matrixBannerCreativeList.size());
                kgUserFilter.pruneKgRes(matrixBannerCreativeList, kgUpFltAsync, myContext.getBtsParameter());
                if(LogUtil.isDebugEnable())LogUtil.debug("yjlin: matrixBannerCreativeList {}", matrixBannerCreativeList.size());
            }


            sw.click(); // clear stop and watch
            rtpWorker.ctrScorer(myContext, matrixItemCreativeList,
                    commonContentCreativeList, matrixBannerCreativeList);

            // after rtp worker, read ctrl results from other tpp solution
            {
                try {
                    final int maxCtrlTimeOut = 2;
                    Map<String, List<CreativeDAO>> openResQue
                            = MatrixReader.getAllResult(matrixGeneralReaderMap, maxCtrlTimeOut, myContext);
                    for (String taskName : openResQue.keySet()) {
                        try {
                            List<CreativeDAO> oneRet = openResQue.get(taskName);
                            switch (taskName) {
                                case MATRIX_GENERAL_CTRL_TASK_NAME: {
                                    matrixBannerCreativeList.addAll(oneRet);
                                    break;
                                }
                                default:
                                    break;
                            }
                        } catch (Exception e) {
                            if(LogUtil.isDebugEnable())LogUtil.info("{}, handle open-api ctrl async result failed{}",
                                    logHeader, Throwables.getStackTraceAsString(e));
                        }
                    }
                } catch (Exception e) {
                    LogUtil.debug("{}, get ctrl result failed {}",
                            logHeader, Throwables.getStackTraceAsString(e));
                }
            }
            for (CreativeDAO dao : matrixBannerCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_rtp";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            {
                Long rt = sw.click();
                /*
                if (myContext.getBtsParameter().isRtCostRecord())
                    MtxTrackLogWorker.addKv("rtp_cost", rt);
                */
                if(LogUtil.isDebugEnable())LogUtil.info("{}, rtp cost {} ms!", logHeader, rt);
            }

            // 精排后，取出U2C
            if (myContext.getBtsParameter().getUseU2cRecall() > 0) {
                rtpWorker.ctrScorer(myContext, Lists.newArrayList(),
                        Lists.newArrayList(), matrixBannerU2cCreativeList);
            }

            // 记录U2C相关的counter
            Map<String, Long> finalU2cpMap = Maps.newHashMap();

            for (int i = 0; i < matrixBannerCreativeList.size(); i++) {
                if (matrixBannerCreativeList.get(i).getExtMap().getOrDefault("mtx_u2cp", -1).equals(1)) {
                    finalU2cpMap.put("u2cp_final.1", finalU2cpMap.getOrDefault("u2cp_final.1", 0L) + 1);
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, c_id {}, u2cp = 1", logHeader, matrixBannerCreativeList.get(i).getCreative_id_uniq());
                } else if (matrixBannerCreativeList.get(i).getExtMap().getOrDefault("mtx_u2cp", -1).equals(0)) {
                    finalU2cpMap.put("u2cp_final.0", finalU2cpMap.getOrDefault("u2cp_final.0", 0L) + 1);
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, c_id {}, u2cp = 0", logHeader, matrixBannerCreativeList.get(i).getCreative_id_uniq());
                } else {
                    finalU2cpMap.put("u2cp_final.-1", finalU2cpMap.getOrDefault("u2cp_final.-1", 0L) + 1);
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, c_id {}, u2cp = -1", logHeader, matrixBannerCreativeList.get(i).getCreative_id_uniq());
                }
            }

            int u2c_index = 0;
            if (myContext.getBtsParameter().getUseU2cRecall() > 0) {
                if (myContext.getBtsParameter().getUseU2cRecall() == 1) {
                    for (int i = 0; i < matrixBannerCreativeList.size(); i++) {
                        if (matrixBannerCreativeList.get(i).getExtMap().getOrDefault("mtx_u2cp", -1).equals(1)) {
                            if (u2c_index < matrixBannerU2cCreativeList.size()) {
                                matrixBannerCreativeList.set(i, matrixBannerU2cCreativeList.get(u2c_index));
                                u2c_index += 1;

                                finalU2cpMap.put("u2cp_replace.1", finalU2cpMap.getOrDefault("u2cp_replace.1", 0L) + 1);
                            }
                        }
                    }
                } else if (myContext.getBtsParameter().getUseU2cRecall() == 2) {
                    for (int i = 0; i < matrixBannerCreativeList.size(); i++) {
                        if (matrixBannerCreativeList.get(i).getExtMap().getOrDefault("x_biz", "null").equals("knowledge") ||
                                matrixBannerCreativeList.get(i).getExtMap().getOrDefault("x_biz", "null").equals("kg_scene")) {
                            if (u2c_index < matrixBannerU2cCreativeList.size()) {
                                matrixBannerCreativeList.set(i, matrixBannerU2cCreativeList.get(u2c_index));
                                u2c_index += 1;

                                finalU2cpMap.put("u2cp_replace.1", finalU2cpMap.getOrDefault("u2cp_replace.1", 0L) + 1);
                            }
                        }
                    }
                }
            }

            if(LogUtil.isDebugEnable())LogUtil.debug("sceneId:{}", myContext.getUrlParameter().getScene());

            if(LogUtil.isDebugEnable())LogUtil.debug("before matrixBannerCreativeList.size: {}", matrixBannerCreativeList.size());

            double rtpTs = System.currentTimeMillis() / 1000.0;

            HashSet<String> openApiAlgTypeSet = Sets.newHashSet();

            {
                sw.click(); // clear stop watch
                final long maxOpenTimeOut = 1;
                try {
                    Map<String, List<CreativeDAO>> openResQue
                            = MatrixReader.getAllResult(matrixOpenReaderMap, maxOpenTimeOut, myContext);
                    for (String taskName : openResQue.keySet()) {
                        try {
                            List<CreativeDAO> oneRet = openResQue.get(taskName);
                            for (CreativeDAO dao : oneRet) {
                                openApiAlgTypeSet.add(dao.getExtMap().getOrDefault("x_biz", "unknown").toString());
                            }
                            switch (taskName) {
                                case MATRIX_LIVE_FEED_TASK_NAME: {
                                    openLiveFeedCreativeList.addAll(oneRet);
                                    break;
                                }
                                case MATRIX_ACTIVITY_TASK_NAME: {
                                    openActivityCreativeList.addAll(oneRet);
                                    break;
                                }
                                case MATRIX_SHOW_TASK_NAME: {
                                    openShowCreativeList.addAll(oneRet);
                                    break;
                                }
                                case MATRIX_HEADLINE_TASK_NAME: {
                                    openHeadlineCreativeList.addAll(oneRet);
                                    break;
                                }
                                case MATRIX_YOUHUI_TASK_NAME: {
                                    openYouHuiCreativeList.addAll(oneRet);
                                    break;
                                }
                                case MATRIX_SHOP_TASK_NAME: {
                                    openShopCreativeList.addAll(oneRet);
                                    break;
                                }
                                default:
                                    break;
                            }
                        } catch (Exception e) {
                            if(LogUtil.isDebugEnable())LogUtil.info("{}, handle open-api async result failed{}",
                                    logHeader, Throwables.getStackTraceAsString(e));
                        }
                    }
                } catch (Exception e) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, get open api result failed {}",
                            logHeader, Throwables.getStackTraceAsString(e));
                }


                // TODO: the following code block needs to be removed once we have working mixed ranking
                if ((!openLiveFeedCreativeList.isEmpty() && !myContext.getBtsParameter().isMixRankOpenApi())
                        || (myContext.getBtsParameter().isMixRankOpenApi() && !myContext.getBtsParameter().getMixRankOpenOnSab().contains(String.valueOf(myContext.getUrlParameter().getMtx_sab())))) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("fix open_api position ");
                    int liveRatio = myContext.getBtsParameter().getLiveShowRatio();
                    int rndNum = ThreadLocalRandom.current().nextInt(100);

                    if (liveRatio > rndNum) {
                        commonContentCreativeList.clear();
                    } else {
                        openLiveFeedCreativeList.clear();
                        openHeadlineCreativeList.clear();
                        openShowCreativeList.clear();
                        openShopCreativeList.clear();
                        openYouHuiCreativeList.clear();
                    }
                }

                openCreativeList.addAll(openLiveFeedCreativeList);
                openCreativeList.addAll(openShowCreativeList);
                openCreativeList.addAll(openShopCreativeList);
                openCreativeList.addAll(openHeadlineCreativeList);
                openCreativeList.addAll(openYouHuiCreativeList);
                Collections.shuffle(openCreativeList);
                commonContentCreativeList.addAll(openCreativeList);

                if(LogUtil.isDebugEnable())LogUtil.debug("commonContentCreativeList info:{}, ", commonContentCreativeList.toString());

                if(LogUtil.isDebugEnable())LogUtil.debug("{}, done reading matrix open api, time {}ms",
                        logHeader, System.currentTimeMillis() - begOpenTsMilis);
                //result.setAttribute("live_result", openLiveFeedCreativeList);
            }

            /*
            if (myContext.getBtsParameter().isRtCostRecord()) {

                long rt = System.currentTimeMillis() - begOpenTsMilis;
                MtxTrackLogWorker.addKv("openapi_cost", rt);
            }
            */

            double openApiTs = System.currentTimeMillis() / 1000.0;

            sw.click(); // clear
            /*
             *
             * mix rank
             *
             * */

            List<CreativeDAO> cttQue = Lists.newArrayList();
            cttQue.addAll(commonContentCreativeList);
            List<CreativeDAO> outMerge = Lists.newArrayList();
            if (myContext.getBtsParameter().getUseBannerUpFilter() > 10) {
                kgUserFilter.pruneKgRes(matrixBannerCreativeList, kgUpFltAsync, myContext.getBtsParameter());
                if(LogUtil.isDebugEnable())LogUtil.debug("yjlin: matrixBannerCreativeList {}", matrixBannerCreativeList.size());
            }

            for (CreativeDAO dao : matrixBannerCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".before_mix";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            /*
            List<CreativeDAO> buyBanners = matrixBuyDirectWorker.getBannerRes(
                    myContext.getUserModel().getGremlinItemTriggerList(),
                    myContext.getUserModel().getAfterBuyConceptInfos(), myContext);
                    */
            List<CreativeDAO> buyBanners = Lists.newArrayList();
            if (myContext.getUrlParameter().getCurPage() == 0 && myContext.getUrlParameter().getScene().equals("88") &&
                    myContext.getBtsParameter().isGremlinRecall() && myContext.getBtsParameter().isGremlinC2cRecall()) {
                if (LogUtil.isDebugEnable()) LogUtil.debug("getBannerResItem trigger {}, concept {}",
                        myContext.getUserModel().getGremlinItemTriggerList(),
                        myContext.getUserModel().getConceptInfos());
                buyBanners = matrixBuyDirectWorker.getBannerResAll(
                        myContext.getUserModel().getGremlinItemTriggerList(),
                        myContext.getUserModel().getConceptInfos(), myContext);
            }

            if ((myContext.getUrlParameter().getScene().equals("88")
                        || myContext.getUrlParameter().getScene().equals("11126"))
                    && myContext.getBtsParameter().isUseNewRankCtrl()) {
                MixRankWorker.rankCtrl(cttQue, buyBanners, matrixBannerCreativeList, outMerge, myContext);
            } else {
                MixRankWorker.rank(cttQue, matrixBannerCreativeList, outMerge, myContext);
            }

            for (CreativeDAO dao : outMerge) {
                if(LogUtil.isDebugEnable())LogUtil.debug("yjlin: {}, field map: {}, {}", dao, dao.getFieldMap(), dao.getFieldMap() == null);
            }
            mergedCreativeList.addAll(outMerge);

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_mix";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            if (myContext.getBtsParameter().getGremlinHisBeRecall()) {
                mergedCreativeList = GremlinReader.filterHisCateBrand(myContext, mergedCreativeList);
            }

            LogXGSync summaryLogger = new LogXGSync();
            Future<Map<Long, SummarySearchItem>> summaryRetAsync
                    = SummaryUtiil.getSummaryAsync(mergedCreativeList, summaryLogger, myContext);

            /*
            {
                Long rt = sw.click();
                if (myContext.getBtsParameter().isRtCostRecord())
                    MtxTrackLogWorker.addKv("mixrank_cost", rt);
                if(LogUtil.isDebugEnable())LogUtil.info("{}, result merge cost {} ms!", logHeader, rt);
            }
            */

            double mixRankTs = System.currentTimeMillis() / 1000.0;

            /*
             *
             * set details
             *
             * */
            AlgorithmDetailWorker algorithmDetailWorker = null;
            Map<String, Future<QueryResult>> ADQueryResultMap = Maps.newHashMap();
            try {
                algorithmDetailWorker = new AlgorithmDetailWorker();
                ADQueryResultMap = algorithmDetailWorker.readAsync(mergedCreativeList, myContext);
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, algo detail worker failed {}", logHeader, Throwables.getStackTraceAsString(e));
            }

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".before_detail";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }
            MandPosInsert.insertMand(mergedCreativeList, myContext.getMandPosBanners(), myContext);

            if (myContext.getBtsParameter().isUseBeerus()
                    && myContext.getUserModel().getUidBucketOth() < myContext.getBtsParameter().getBeerusRatio()) {
                BeerusDetails.fillDetail(myContext, mergedCreativeList);
            } else {
                DispatchSpetialExecuteFactory.getDefaultExecute().execute(
                        myContext, mergedCreativeList);
            }

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_detail";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }
            if(LogUtil.isDebugEnable())LogUtil.debug("{} after detail worker, left ids: [{}]",
                    logHeader, Lists.transform(mergedCreativeList, CreativeDAO::getCreative_id_uniq));

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".before_title_filter";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_title_filter";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            //认知图谱商品副标题
            final long recTitleWorkerTsMilis = System.currentTimeMillis();
            try {
                RecTitleWorker recTitleWorker = new RecTitleWorker();
                if (myContext.getBtsParameter().isNeedOnlineRecSubTitle()
                        && !myContext.getBtsParameter().isShutdownMost()) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("enter recTitleWorker");
                    recTitleWorker.worker(myContext, mergedCreativeList);
                }
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, recTitleWorker failed {}",
                        logHeader, Throwables.getStackTraceAsString(e));
            }
            if(LogUtil.isDebugEnable())LogUtil.debug("recTitleWorker cost {} ms!", System.currentTimeMillis() - recTitleWorkerTsMilis);
            if(LogUtil.isDebugEnable())LogUtil.debug("{} after recTitleWorker, left ids: [{}]",
                    logHeader, Lists.transform(mergedCreativeList, CreativeDAO::getCreative_id_uniq));

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_subtitle";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            //测试新卡片
            try {
                NewTypeWorker newTypeWorker = new NewTypeWorker();
                if (myContext.getBtsParameter().isNeedNewType4Knowledge()) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("enter NewTypeWorker");
                    newTypeWorker.worker(myContext, mergedCreativeList);
                }
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, NewTypeWorker failed {}", logHeader, Throwables.getStackTraceAsString(e));
            }
            if(LogUtil.isDebugEnable())LogUtil.debug("{} after newTypeWorker, left ids: [{}]",
                    logHeader, Lists.transform(mergedCreativeList, CreativeDAO::getCreative_id_uniq));

            // find sim to creativeList
            try {
                algorithmDetailWorker.execute(ADQueryResultMap, mergedCreativeList, myContext);
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, find sim err {}", logHeader, Throwables.getStackTraceAsString(e));
            }

            if(LogUtil.isDebugEnable())LogUtil.debug("{} after fs detail, left ids: [{}]",
                    logHeader, Lists.transform(mergedCreativeList, CreativeDAO::getCreative_id_uniq));

            if(LogUtil.isDebugEnable())LogUtil.debug("after mergedCreativeList.size: {}", mergedCreativeList.size());
            if(LogUtil.isDebugEnable())LogUtil.debug("after mergedCreativeList.info: {}", mergedCreativeList.toString());
            /*
            {
                Long rt = sw.click();
                if (myContext.getBtsParameter().isRtCostRecord())
                    MtxTrackLogWorker.addKv("detail_cost", rt);
                if(LogUtil.isDebugEnable())LogUtil.info("{}, content detail cost {} ms!", logHeader, rt);
            }
            */

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".before_cut";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            SummaryUtiil.filterResBySummary(mergedCreativeList, summaryRetAsync, summaryLogger, myContext);

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".get_summary";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            try {
                if (myContext.getBtsParameter().isNeedFilterGulDisLike()) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("enter DisLikeFilterWorker");
                    DisLikeFilterWorker.worker(myContext, mergedCreativeList);
                }
            } catch (Exception e) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, DisLikeFilterWorker failed {}",
                        logHeader, Throwables.getStackTraceAsString(e));
            }

            // real cut
            setTkPos(mergedCreativeList,myContext);

            MixRankWorker.cut(mergedCreativeList, myContext);
            double detailTs = System.currentTimeMillis() / 1000.0;
            if(LogUtil.isDebugEnable())LogUtil.debug("{} after cut, left ids: [{}]",
                    logHeader, Lists.transform(mergedCreativeList, CreativeDAO::getCreative_id_uniq));

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_cut";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            // why insertMand is c alled here?
            // because we need to ensure mandPosBanner's existence after MixRankWork
            MandPosInsert.insertMand(mergedCreativeList, myContext.getMandPosBanners(), myContext);
            MatrixRetShaperTemp.writeHistBannerItemAsync(myContext, mergedCreativeList);
            if (myContext.getBtsParameter().isDebugMork() && mergedCreativeList.size() > 0) {
                if(LogUtil.isDebugEnable())LogUtil.debug("{}, in debug mork", logHeader);
                //int rndNum = ThreadLocalRandom.current().nextInt(mergedCreativeList.size());
                for (int i = 0; i < mergedCreativeList.size(); ++i) {
                    mergedCreativeList.get(i).getExtMap().put("mtx_pos", i + 1);
                }
            }
            if (matrixItemCreativeList != null) {
                try {
                    for (CreativeDAO dao : matrixItemCreativeList) {
                        dao.getExtMap().put("x_biz", "item");
                        dao.getExtMap().put("x_item_ids", dao.getCreative_id());
                    }
                } catch (Exception e) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, insert item log failed",
                            logHeader, Throwables.getStackTraceAsString(e));
                }
            }
            mergedCreativeList.addAll(matrixItemCreativeList);

            double otherTs = System.currentTimeMillis() / 1000.0;

            if (mergedCreativeList == null) {
                mergedCreativeList = Lists.newArrayList();
            }
            if (myContext.getUrlParameter().isNeedMork()) {
                if(LogUtil.isDebugEnable())LogUtil.info("need_mork is true", logHeader, sw.click());
                mergedCreativeList.addAll(MatrixMorkReader.read(myContext));
                //MatrixMorkReader.morkDec(mergedCreativeList);
            }

            if (mergedCreativeList != null && !mergedCreativeList.isEmpty()) {
                for (int i = 0; i < mergedCreativeList.size(); ++i) {
                    mergedCreativeList.get(i).getExtMap().put("x_org_pos", i);
                }
            }

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_pos";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            // add counter of discovery
            if ((myContext.getUrlParameter().getScene().equals("88")
                        || myContext.getUrlParameter().getScene().equals("11126"))
                    && myContext.getBtsParameter().isUseNewRankCtrl()) {
                double dweight = 1.0;
                for (CreativeDAO dao : mergedCreativeList) {
                    MixRankWorker.typeSpecDiscCounterAdd(myContext.getUrlParameter().getScene(),
                            context.getCurrentAbId(),
                            dao.getExtMap().getOrDefault("x_biz", "null").toString(),
                            (int) dao.getExtMap().getOrDefault("mtx_scate", 1), 1.0 / dweight);
                    MixRankWorker.typeSpecCounterAdd(myContext.getUrlParameter().getScene(),
                            context.getCurrentAbId(),
                            dao.getExtMap().getOrDefault("x_biz", "null").toString(),
                            1.0 / dweight);
                    //dweight /= myContext.getBtsParameter().getBannerPosBias();
                    dweight += 1;
                }
            }

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_dweight";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }

            if (godResList != null) {
                try {
                    GodsWishWorker.insertGodWish(mergedCreativeList, godResList);
                } catch (Exception e) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, parse godResAsync failed {}",
                            logHeader, Throwables.getStackTraceAsString(e));
                }
            }

            if (mergedCreativeList != null) {
                try {
                    mergedCreativeList.removeIf(
                            obj -> !obj.getExtMap().get("x_biz").equals("topic_activity_pic")
                                    && !obj.getExtMap().getOrDefault("mtx_biz_type", "").equals("open")
                                    && !obj.getExtMap().containsKey("x_item_ids")
                    );
                } catch (Exception e) {
                    if(LogUtil.isDebugEnable())LogUtil.debug("{}, remove illegal result failed {}",
                            logHeader, Throwables.getStackTraceAsString(e));
                }
            }

            //MandPosInsert.insertIphone(mergedCreativeList, myContext);
            MixRankWorker.decorate(mergedCreativeList, myContext);
            result.setRecommendResult(mergedCreativeList);
            /*
            if (myContext.getBtsParameter().isRtCostRecord()) {
                Long endTsLong = System.currentTimeMillis();
                MtxTrackLogWorker.addKv("total_cost", endTsLong - startTsLong);
            }
            */
            result.setTrackLog(MtxTrackLogWorker.getTrackLog(mergedCreativeList, myContext));

            boolean kgDebug = ParameterUtil.getParameterAsBoolean(context, "kg_debug", false);
            if (kgDebug) {
                result.setAttribute("u2c_result", myContext.getUserModel().getConceptInfos());
            }

            for (CreativeDAO dao : mergedCreativeList) {
                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                key = key + ".after_kg_debug";
                detailTypeMap.put(key, detailTypeMap.getOrDefault(key, 0L) + 1);
            }
            /*
            // add track log
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(5);
            nf.setRoundingMode(RoundingMode.HALF_UP);
            String nodeMapStr = myContext.getUserModel().getNodeIntentionMap().entrySet().stream()
                    .map(entry -> entry.getKey() + ":" + nf.format(entry.getValue())).collect(Collectors.joining(","));
            result.setTrackLog(nodeMapStr);
            */

            Map<String, Long> tppCntMap = Maps.newHashMap();
            tppCntMap.put("knowledge", 0L);
            tppCntMap.put("topic_activity_item", 0L);
            tppCntMap.put("topic_czjx", 0L);
            tppCntMap.put("topic_activity_pic", 0L);
            tppCntMap.put("topic_bd", 0L);

            Map<String, Long> noFieldMapCounterMap = Maps.newHashMap();

            Set<String> idstSet = Sets.newHashSet();
            for (CreativeDAO dao: mergedCreativeList) {
                idstSet.add(dao.getExtMap().getOrDefault("virt_cat", "").toString());
            }
            tppCntMap.put("virt_cat", Long.valueOf(idstSet.size()));

            Long topic_activity_pic_counter = 0L;
            for (CreativeDAO dao : mergedCreativeList) {

                String key = dao.getExtMap().containsKey("x_biz")
                        ? dao.getExtMap().get("x_biz").toString() : dao.getContent_type().toString();
                tppCntMap.put(key, tppCntMap.getOrDefault(key, 0L) + 1);

                if (dao.getFieldMap() == null) {
                    noFieldMapCounterMap.put(key, noFieldMapCounterMap.getOrDefault(key, 0L) + 1);
                }


                if (!myContext.getBtsParameter().getDebugCountPlanId().equals("") && myContext.getBtsParameter().getDebugCountPlanId().equals(dao.getPlan_id())) {
                    topic_activity_pic_counter++;
                }

                if (dao.getExtMap().getOrDefault("mtx_u2cp", -1).equals(1)) {
                    finalU2cpMap.put("u2cp_final_final.1", finalU2cpMap.getOrDefault("u2cp_final_final.1", 0L) + 1);
                }

                if (dao.getExtMap().getOrDefault("mtx_is_event", -1).equals(1)) {
                    finalU2cpMap.put("is_event_1", finalU2cpMap.getOrDefault("is_event_1", 0L) + 1);
                } else if (dao.getExtMap().getOrDefault("mtx_is_event", -1).equals(2)) {
                    finalU2cpMap.put("is_event_2", finalU2cpMap.getOrDefault("is_event_2", 0L) + 1);
                } else if (dao.getExtMap().getOrDefault("mtx_is_event", -1).equals(3)) {
                    finalU2cpMap.put("is_event_3", finalU2cpMap.getOrDefault("is_event_3", 0L) + 1);
                }

                if (!myContext.getContext().getDowngradeLevel().equals("LEVEL3")) {
                    try {
                        if (dao.getExtMap().getOrDefault("mtx_replace", -1).equals(1)) {
                            finalU2cpMap.put("mtx_replace", finalU2cpMap.getOrDefault("mtx_replace", 0L) + 1);
                        }
                    } catch (Exception e) {
                        if (LogUtil.isDebugEnable()) LogUtil.error("Exception:{}", Throwables.getStackTraceAsString(e));
                    }
                }
            }

            if (!myContext.getBtsParameter().getDebugCountPlanId().equals(""))
                ServiceFactory.getTPPCounter().countAvg(myContext.getUrlParameter().getScene() + ".plan." + myContext.getBtsParameter().getDebugCountPlanId() + ".size", topic_activity_pic_counter);

            for (String objectType : tppCntMap.keySet()) {
                ServiceFactory.getTPPCounter().countAvg(myContext.getUrlParameter().getScene() + "." + objectType + ".size"
                        , tppCntMap.get(objectType));
            }

            if (myContext.getContext().getDowngradeLevel().equals("LEVEL0")) {
                for (String objectType : noFieldMapCounterMap.keySet()) {
                    ServiceFactory.getTPPCounter().countAvg(myContext.getUrlParameter().getScene() + ".no_field_map." + objectType + ".size"
                            , noFieldMapCounterMap.get(objectType));
                }
            }

            if (myContext.getContext().getDowngradeLevel().equals("LEVEL0")) {
                for (String objectType : finalU2cpMap.keySet()) {
                    ServiceFactory.getTPPCounter().countAvg(myContext.getUrlParameter().getScene() + "." + objectType
                            , finalU2cpMap.get(objectType));
                }
            }
            for (String objectType : detailTypeMap.keySet()) {
                ServiceFactory.getTPPCounter().countAvg(myContext.getUrlParameter().getScene() + "." + objectType
                        , detailTypeMap.get(objectType));
            }
            ServiceFactory.getTPPCounter().countAvg(myContext.getUrlParameter().getScene() + ".size"
                    , mergedCreativeList.size());

        } catch (Exception e) {
            if(LogUtil.isDebugEnable())LogUtil.error("Exception:{}", Throwables.getStackTraceAsString(e));
        }
    }

    private static void setTkPos(List<CreativeDAO> mergedCreativeList, MyContext myContext) {
        String logHeader = "setTkPos";
        try {
            List<CreativeDAO> mergedCttCreativeList = Lists.newArrayList();
            List<CreativeDAO> mergedTkCreativeList = Lists.newArrayList();
            List<CreativeDAO> resCreativeList = Lists.newArrayList();

            List<Pair<CreativeDAO, Double>> bannerTkItemRtpScore = Lists.newArrayList();

            if(myContext.getBtsParameter().isNeedTkSetPos()&& StringUtil.isNotBlank(myContext.getBtsParameter().getTkIndex()))
            {
                for(CreativeDAO dao: mergedCreativeList){

                    String mtx_biz_type = "com_ctt";
                    try{
                        mtx_biz_type = dao.getExtMap().getOrDefault("mtx_biz_type", "com_ctt").toString();
                    }
                    catch (Exception e){
                        if(LogUtil.isDebugEnable())LogUtil.debug("mtx_biz_type error: {} ", e.toString());
                    }
                    if(mtx_biz_type.equals("disp_ctrl")){
                        mergedTkCreativeList.add(dao);
                        double score =Double.parseDouble(dao.getExtMap().getOrDefault("mtx_ctr", 0.0).toString()) ;
                        bannerTkItemRtpScore.add(Pair.of(dao, score));
                    }else {
                        mergedCttCreativeList.add(dao);
                    }
                }
                if(LogUtil.isDebugEnable())LogUtil.debug("{},step1 mergedTkCreativeList {},mergedCttCreativeList {}", logHeader, mergedTkCreativeList.size(),mergedCttCreativeList.size());

                bannerTkItemRtpScore.sort((o1, o2) -> (o2.snd.compareTo(o1.snd)));
                Map<Integer, Integer> tkIndexMap = Maps.newHashMap();
                String[] tkIndexArray = StringUtil.split(myContext.getBtsParameter().getTkIndex(), ';');
                List<Integer> tkIndexList = Lists.newArrayList();
                int page = myContext.getUrlParameter().getCurPage();

                for (String str: tkIndexArray) {
                    String[] ctrlP = StringUtil.split(str, ':');
                    if (2 == ctrlP.length&&Integer.parseInt(ctrlP[0]) == page) {
                        tkIndexList.add(  Integer.parseInt(ctrlP[1]));
                    }
                }
//                int index=0;
                int TkIndex = 0;
                if(LogUtil.isDebugEnable())LogUtil.debug("{},step2 tkIndexList {}", logHeader, tkIndexList);
                resCreativeList.addAll(mergedCttCreativeList);
                for(Integer ind:tkIndexList){
                    if(mergedTkCreativeList.size()>TkIndex){
                        resCreativeList.add(ind,mergedTkCreativeList.get(TkIndex));
                        TkIndex++;
                    }

                }

//                for(CreativeDAO dao: mergedCttCreativeList){
////                    if(tkIndexList.contains(index) && mergedTkCreativeList.size()>TkIndex){
////                        resCreativeList.add(mergedTkCreativeList.get(TkIndex));
////                        TkIndex++;
////                        index++;
////                    }
////                    resCreativeList.add(dao);
////                    index++;
////                }
                mergedCreativeList.clear();
                mergedCreativeList.addAll(resCreativeList);

                if(LogUtil.isDebugEnable())LogUtil.debug("{},step3 mergedCreativeList size {}", logHeader, mergedCreativeList.size());

            }
        }catch (Exception e){
            if(LogUtil.isDebugEnable())LogUtil.error("{},Exception:{}",logHeader, Throwables.getStackTraceAsString(e));

        }
    }
}